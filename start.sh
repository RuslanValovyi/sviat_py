#!/bin/bash

MYPATH=/home/pi/Adeept_AWR/sviat

if [ "$#" -ne 1 ]; then
    echo "You must enter your script name: start.sh <script name>"
else
    scp $1 raspberry:$MYPATH/
    echo "*** Started."
    ssh -t raspberry "cd $MYPATH && sudo python3 -u $1"
    echo "*** Finished."
fi
