#!/usr/bin/python3

# File name   : sviat_parktronik.py
# Description : work with sviat_parktronik 

import RPi.GPIO as GPIO
import time
import move
import LED
from rpi_ws281x import *
import ultra
import findline
import signal, os

is_running = True
def handler(signum, frame):
    global is_running
    is_running = False

# Set the signal handler
signal.signal(signal.SIGINT, handler)

# my_move(timer=10, speed=70, direction='forward', left_right='no')
def my_move(timer, speed, direction, left_right):
    while timer>0:
        move.move(speed, direction, left_right, 1)
        timer = timer - 1
        time.sleep(0.2)

# ==================================================================
if __name__ == '__main__':
    led = LED.LED()

    try:
        led.colorWipe(Color(5,5,5))
        ultra.setup()
        move.setup()

        speed = 80
        dirrection = move.DIR_FORWARD
        turn=move.TURN_NO
        cntr = 0

        while(is_running == True):
            dist = round(ultra.checkdist(),2)
            if dist <= 0.7:
                speed = 70
                dirrection = move.DIR_BACKWARD
#                turn = move.TURN_LEFT
                turn = move.TURN_RIGHT
            else:
                speed = 80
                dirrection = move.DIR_FORWARD
                turn = move.TURN_NO
            move.move(speed, dirrection, turn)

            time.sleep(0.01)

            # debug
            cntr+=1
            if cntr == 100:
                print("Result: ", str(dist))
                cntr = 0

    except Exception as err:
        print(f"### Exception: {err}")

    led.colorWipe(Color(0,0,0))
    move.destroy()
    print("### Parktronik Finished")
